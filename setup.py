#!/usr/bin/env python

"""
setup.py as suggested by setuprools for compatibility with legacy
builds or certain versions of tools.
The whole configuration is un pyproject.toml.

See https://setuptools.pypa.io/en/stable/userguide/pyproject_config.html
"""

from setuptools import setup

setup()
