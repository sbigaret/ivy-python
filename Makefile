SHELL := bash
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

SRCS_DIR=ivy examples
TESTS_DIR=tests

ISORT_OPTS=--atomic --combine-star --use-parentheses \
	-m VERTICAL_HANGING_INDENT -w 80

.PHONY: isort-check lint test apidocs build upload clean distclean

isort-check:
	isort --atomic ${ISORT_OPTS} -q --diff setup.py ${SRCS_DIR} ${TESTS_DIR}
	@echo "Remove --diff to do it"

lint:
	flake8 ivy tests
	isort --check-only ${ISORT_OPTS} -q setup.py ${SRCS_DIR} ${TESTS_DIR}
	mypy --allow-redefinition --disallow-untyped-defs ivy

test:
	python3 -m pytest tests

htmldoc:
	cd doc && make html

apidocs:
	pydoctor --docformat restructuredtext ivy

build:
	git diff-index --quiet HEAD --  \
		|| (git status && echo "ERROR: Working dir. is dirty" && exit 1)
	python3 -m build

upload:
	python3 -m twine upload --repository testpypi dist/*

clean:
	-find . -type d -name '__pycache__' -exec rm --recursive --force {} +

distclean: clean
	rm -rf dist/ivy-python*.tar.gz dist/ivy-python*.whl
