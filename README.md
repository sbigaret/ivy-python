Ivy: a lightweight software bus
===============================

[Ivy](https://www.eei.cena.fr/products/ivy/) is a lightweight software
bus for quick-prototyping protocols.  It allows applications to
broadcast information through text messages, with a subscription
mechanism based on regular expressions.

This is the python implementation.  A lot of materials, including
libraries for many other languages are available from the [Ivy Home
Page](https://www.eei.cena.fr/products/ivy/).

Supported versions of python are: 3.7+

> **Note**: ivy-python v3.3 was the latest version supporting Python 2.7.


Contributors
------------

The following people have participated to the project, by reporting
bugs, proposing new features and/or proposing patches, proposing the
hosting svn & web servers (before we switched to gitlab), etc.

By chronological  order:

- Frédéric Cadier
- Marcellin Buisson
- Yannick Jestin
- Olivier Fourdan
- Olivier Saal
- Gwenael Bothorel
- Jiri Borovec
- Nicolas Courtel
- Fabien André
- Bohdan Mushkevych
- Antoine Drouin
- Felix Ruess
- Aurélien Pardon
- Duminda Ratnayake
- Mathieu Cousy
- Fabien-B
- Alexandre Duchevet

Thank you!
